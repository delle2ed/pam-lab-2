import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { TabNavigator } from 'react-navigation';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';


class Daily extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: `Calendar`,
  });
  render() {
    const { navigate } = this.props.navigation;
    return (
        <Calendar/>
    );
  }
}

class Planer extends React.Component {
  // Nav options can be defined as a function of the screen's props:
  static navigationOptions = ({ navigation }) => ({
    title: `Planer`,
  });
  render() {
    // The screen's current route is passed in to `props.navigation.state`:
    const { params } = this.props.navigation.state;
    return (
   
       <Agenda
       // the list of items that have to be displayed in agenda. If you want to render item as empty date
       // the value of date key kas to be an empty array []. If there exists no value for date key it is
       // considered that the date in question is not yet loaded
       items={
         {'2017-10-24': [{text: 'item 1 - any js object'}],
          '2017-10-27': [{text: 'item 2 - any js object'}],
          '2017-10-26': [],
          '2017-10-25': [{text: 'item 3 - any js object'},{text: 'any js object'},{text: 'hello'}],
         }}
       // callback that gets called when items for a certain month should be loaded (month became visible)
       loadItemsForMonth={(month) => {console.log('trigger items loading')}}
       // callback that gets called on day press
       onDayPress={(day)=>{console.log('day pressed')}}
       // callback that gets called when day changes while scrolling agenda list
       onDayChange={(day)=>{console.log('day changed')}}
       // initially selected day
       // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
       minDate={'2016-10-10'}
       // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
       maxDate={'2017-18-30'}
       // Max amount of months allowed to scroll to the past. Default = 50
       pastScrollRange={50}
       // Max amount of months allowed to scroll to the future. Default = 50
       futureScrollRange={50}
       // specify how each item should be rendered in agenda
       renderItem={(item, firstItemInDay) => {return (<View />);}}
       // specify how each date should be rendered. day can be undefined if the item is not first in that day.
       renderDay={(day, item) => {return (<View />);}}
       // specify how empty date content with no items should be rendered
       renderEmptyDate={() => {return (<View />);}}
       // specify how agenda knob should look like
       renderKnob={() => {return (<View />);}}
       // specify your item comparison function for increased performance
       rowHasChanged={(r1, r2) => {return r1.text !== r2.text}}
       // Hide knob button. Default = false
       hideKnob={false}
       onDayPress={(day) => {console.log('selected day', day)}}
       markedDates={{
    '2012-05-16': {selected: true, marked: true},
    '2012-05-17': {marked: true},
  }}
       // By default, agenda dates are marked if they have at least one item, but you can override this if needed
       // agenda theme
       // agenda container style
       
       style={{}}
     />
    );
  }
}

export const MainScreenNavigator = TabNavigator({
  Daily: { screen: Daily },
  Planer: { screen: Planer },
});

export default class App extends React.Component {
  render() {
    return <MainScreenNavigator />;
  }
}
;
